import Vue from 'vue';
import Vuex from 'vuex';
import axios from 'axios';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    users: [],
    modals: {
      // edit
      editModalIsOpen: false,
      editModalPayload: {},
      // delete
      deleteModalIsOpen: false,
      deleteModalPayload: {}
    }
  },
  mutations: {
    // def
    initData: (state) => {
      if(localStorage.fakeUsers){        
        state.users = JSON.parse(localStorage.getItem('fakeUsers'));
      }
    },
    // modals
    changeStateEditModal: (state, arg) => {
      state.modals.editModalIsOpen = arg;
    },
    changeStateDeleteModal: (state, arg) => {
      state.modals.deleteModalIsOpen = arg;
    },
    setPayloadEditModal: (state, arg) => {
      state.modals.editModalPayload = arg;
    },
    setPayloadDeleteModal: (state, arg) => {
      state.modals.deleteModalPayload = arg;
    },
    // others
    addFakeUsers(state){
      axios.get('https://reqres.in/api/users').then(resp => {
        state.users = resp.data.data;
        localStorage.setItem('fakeUsers', JSON.stringify(state.users));
      });
    },
    addUserNote(state, obj){
      state.users.push(obj);
      localStorage.setItem('fakeUsers', JSON.stringify(state.users));
    },
    changeUserNote(state, newObj){
      state.users.forEach((element, index) => {
        if(element.id === newObj.id) {
          state.users.splice(index, 1, newObj);
          localStorage.setItem('fakeUsers', JSON.stringify(state.users));
        }
      });      
    },
    deleteUserNote(state, user){
      state.users.forEach((element, index) => {
        if(element.id === user.id) {
          state.users.splice(index, 1);
          localStorage.setItem('fakeUsers', JSON.stringify(state.users));
        }
      });
    },
    deleteAllNotes(state){
      state.users = [];
      localStorage.setItem('fakeUsers', JSON.stringify(state.users));
      localStorage.clear();
    },
  },
  actions: {
    // def
    initData: (context) => {
      context.commit('initData');
    },
    // modals
    changeStateEditModal: (context, arg) => {
      context.commit('changeStateEditModal', arg);
    },
    changeStateDeleteModal: (context, arg) => {
      context.commit('changeStateDeleteModal', arg);
    },
    setPayloadEditModal: (context, arg) => {
      context.commit('setPayloadEditModal', arg);
    },
    setPayloadDeleteModal: (context, arg) => {
      context.commit('setPayloadDeleteModal', arg);
    },
    // others
    addFakeUsers: (context) => {
      context.commit('addFakeUsers');
    },
    addUserNote: (context, obj) => {
      context.commit('addUserNote', obj);
    },
    changeUserNote: (context, newObj) => {     
      context.commit('changeUserNote', newObj);
    },
    deleteUserNote: (context, user) => {
      context.commit('deleteUserNote', user);
    },
    deleteAllNotes: (context) => {
      context.commit('deleteAllNotes');
    }
  },
  getters: {
    // users
    users(state) {
      return state.users;
    },
    // modals
    editModalState(state) {
      return state.modals.editModalIsOpen;
    },
    deleteModalState(state) {
      return state.modals.deleteModalIsOpen;
    },
    editModalPayload(state) {
      return state.modals.editModalPayload;
    },
    deleteModalPayload(state) {
      return state.modals.deleteModalPayload;
    }
  }
})
